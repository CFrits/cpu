#!/usr/bin/ruby

IRIE  = 1
PCOE  = 2
PCIE  = 4
PCCNT = 8
RESOE = 16
CIN   = 32
ALU0  = 64
ALU1  = 128
TMPIE = 256
ACCOE = 512
ACCIE = 1024
MEMOE = 2048
MARIE = 4096
OUTOE = 8192
OUTIE = 16384

# Instruction 00 LDA immediate
# 
# The 4 clock periods of a LDA immediate instruction:
#
# 1. PCOE MARIE
# 2. MEMOE IRIE PCCNT
#
# 3. PCOE MARIE
# 4. MEMOE ACCIE PCCNT

print("[\n")

# Instruction 00 LDA immediate:	    A = MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
    PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + ACCIE + PCCNT, 0, 0, 0, 0)

# Instruction 01 ADD immediate:     A = A + MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
    PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + TMPIE + PCCNT,ACCOE + ALU0 + ALU1,RESOE + ACCIE, 0, 0)

# Instruction 02 OUT implied:	    OUT = A
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,ACCOE + OUTIE, 0, 0, 0, 0, 0)

# Instruction 03 JMP absolute:	    JMP MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + PCIE + PCCNT, 0, 0, 0, 0)

# Instruction 04 SBIT immediate:    OUT[Bit] = 1
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + TMPIE + PCCNT, OUTOE + ACCIE, ACCOE + ALU0, RESOE + OUTIE, 0)

# Instruction 05 CBIT immediate:    OUT[Bit] = 0
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\"\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + TMPIE + PCCNT, OUTOE + ACCIE, ACCOE, RESOE + OUTIE, 0)

print("]\n")
