This version adds the following:

* The connections from the IR are only going to the Decoder, not to the ALU anymore.
  The ALU is now controlled via the decoder. This makes the microcode instruction location
  independent from the ALU function.

* An extra instruction bit has been added bringing the total possible micro instructions to 8.

* A buffer has been added to the output of the "OUT" register so the content of the
  register can be read back.

* A status register has been added with currently only a carry bit.

* The ALU now has a carry in signal.

* The "AND", "OR" and "NOT" functions have been added to the ALU.

* The CBIT (Clear bit) and SBIT (Set bit) instructions have been added in the microcode.


Set bit in OUT register:

    Loop:
        SBIT #0x02  Out = Out | #0x02
        JMP Loop

Clear a bit in the OUT register. Observe that the remaining bits are untouched.

    Loop:
        LDA  #0XFF
        OUT
        CBIT #0xFD  Out = Out & #0xFD
        JMP Loop

Observe the Carry bit:

    Loop:
        LDA  #0xC0
        ADD  #0xC0
        JMP Loop
