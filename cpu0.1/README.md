This is one of most simple CPU I could think of. It uses an Accumulator based architecture.
This means that the destination or source of an instruction is always the Accumulator. 
The goal was not to create the most simple CPU, but the first version of a CPU which could easily be extended.
This first version only has 4 instructions and the Arithmetic and Logic Unit (ALU) has one function, the ADD function.

The four instructions are:

* LOAD		Load Accumulator with a constant from memory.
* ADD		Add to the Accumulator a constant from memory.
* OUT		Store the content of the Accumulator in the Output register.
* JUMP		Jump to an address locations from memory.

A simple program would be a counter:

	   LOAD #0x55   Accu = 0x55
    Loop:
	   ADD  #0x01   Accu = Accu + 1
	   OUT          Output = Accu
	   JUMP Loop


Another program which is a running LED:

    Loop:
	   LOAD #0x01
	   OUT
	   LOAD #0x02
	   OUT
	   LOAD #0x04
	   OUT
	   LOAD #0x08
	   OUT
	   LOAD #0x10
	   OUT
	   LOAD #0x20
	   OUT
	   LOAD #0x40
	   OUT
	   LOAD #0x80
	   OUT
	   JUMP Loop

Traffic light:

    Loop:
	   LOAD #0x01
	   OUT
	   LOAD #0x02
	   OUT
	   LOAD #0x04
	   OUT
	   JUMP Loop
