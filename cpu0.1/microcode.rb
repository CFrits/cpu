#!/usr/bin/ruby

PCIE  = 1
PCOE  = 2
PCCNT = 4
MARIE = 8
IRIE  = 16
MEMOE = 32
ACCIE = 64
ACCOE = 128
TMPIE = 256
RESOE = 512
OUTIE = 1024

# Instruction 00 LDA immediate
# 
# The 4 clock periods of a LDA immediate instruction:
#
# 1. PCOE MARIE                      0x002 + 0x008          = 0x00A
# 2. MEMOE IRIE PCCNT                0x020 + 0x010 + 0x004  = 0x034
#
# 3. PCOE MARIE                                             = 0x00A
# 4. MEMOE ACCIE PCCNT               0x020 + 0x040 + 0x004  = 0x064

print("[\n")

# Instruction 00 LDA immediate:	A = MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + ACCIE + PCCNT, 0, 0, 0, 0)

# Instruction 01 ADD immediate:	A = A + MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + TMPIE + PCCNT,ACCOE,RESOE + ACCIE, 0, 0)

# Instruction 02 OUT implied:	OUT = A
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,ACCOE + OUTIE, 0, 0, 0, 0, 0)

# Instruction 03 JMP absolute:	JMP MEM[PC]
printf("\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\",\"0x%03X\"\n", 
	PCOE + MARIE,MEMOE + IRIE + PCCNT,PCOE + MARIE,MEMOE + PCIE + PCCNT, 0, 0, 0, 0)

print("]\n")
